// ***************** */
//  BASE STYLE       */
// ***************** */
const mainColor = '#333';
const backgroundColor = '#bdb'; // choose null if non-transparent background image should be used
// const highlightColor = '#ee3300';

const appStyle = `background-color:${backgroundColor};color:${mainColor};fill:${mainColor}`;

// ***************** */
//  OVERRIDE STYLES  */
// ***************** */

//* **  HEADER **/
const headerStyle = 'background: rgba(255, 255, 255, 0.5);';
const logoStyle = '';

//* **  CLOCK **/
const clockStyle = ``;

//* **  PRAYERS **/
const prayersStyle = ``;
const prayerRowStyle = ``;
const prayerHeaderStyle = ``;
const prayerMainStyle = ``;
const prayerNextStyle = ``;

//* **  MESSAGE **/
const messageStyle = '';
const messageTextStyle = 'font-size:80%;font-weight:700;';
const messageRefStyle = 'font-size:75%;';

//* **  COUNTDOWN **/
const countdownStyle = ``;
const barStyle = '';
const barBgStyle = '';

//* **  FOOTER **/
const footerStyle = '';

export default appStyle;
export {
  appStyle,
  headerStyle,
  logoStyle,
  clockStyle,
  prayersStyle,
  prayerRowStyle,
  prayerHeaderStyle,
  prayerMainStyle,
  prayerNextStyle,
  messageStyle,
  messageTextStyle,
  messageRefStyle,
  countdownStyle,
  barStyle,
  barBgStyle,
  footerStyle,
};
